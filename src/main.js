import Vue from 'vue'
import App from './App.vue'
import About from './components/About.vue'
import Home from './components/Home.vue'
import Skill from './components/Skill.vue'
import Contact from './components/Contact.vue'
import Router from 'vue-router'
import 'vue-progress-path/dist/vue-progress-path.css'
import  './js/background.js'
import VueProgress from 'vue-progress-path'
import  './style/style.scss'

Vue.use(VueProgress);
Vue.use(Router);

const router = new Router ({
    mode: 'history',
    routes: [
        {
          path: '/',
          name: 'Home',
          component: Home
        },
        {
            path: '/about',
            name: 'About',
            component: About
        },
        {
            path: '/skill',
            name: 'Skill',
            component: Skill
        },
        {
            path: '/contact',
            name: 'Contact',
            component: Contact
        }
    ]
});

new Vue({
  el: '#app',
  render: h => h(App),
  router
});